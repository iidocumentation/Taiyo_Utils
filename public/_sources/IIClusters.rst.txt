IIClusters package
==================

Submodules
----------

IIClusters.iiDTW module
-----------------------

.. automodule:: IIClusters.iiDTW
    :members:
    :undoc-members:
    :show-inheritance:

IIClusters.iiUmap module
------------------------

.. automodule:: IIClusters.iiUmap
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: IIClusters
    :members:
    :undoc-members:
    :show-inheritance:
