Taiyo\_Clusters package
=======================

.. automodule:: Taiyo_Clusters
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

.. automodule:: Taiyo_Clusters.toDTW
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: Taiyo_Clusters.toUmap
    :members:
    :undoc-members:
    :show-inheritance:


