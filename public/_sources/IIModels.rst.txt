IIModels package
================

Subpackages
-----------

.. toctree::

    IIModels.NeuralNets
    IIModels.TSmodels

Submodules
----------

IIModels.iiClassifiers module
-----------------------------

.. automodule:: IIModels.iiClassifiers
    :members:
    :undoc-members:
    :show-inheritance:

IIModels.iiEnsemble module
--------------------------

.. automodule:: IIModels.iiEnsemble
    :members:
    :undoc-members:
    :show-inheritance:

IIModels.iiFilters module
-------------------------

.. automodule:: IIModels.iiFilters
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: IIModels
    :members:
    :undoc-members:
    :show-inheritance:
